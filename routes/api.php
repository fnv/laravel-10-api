<?php

use App\Http\Controllers\Api\OperatorController;
use App\Http\Controllers\Api\SupportController;
use App\Http\Controllers\Api\StatusImplantationController;
use App\Http\Controllers\Api\ServiceStatusController;
use App\Http\Controllers\Api\AttendSendProposalStatusController;
use App\Http\Controllers\Api\AttendSendProposalTypeSendingController;
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\PlanController;
use App\Http\Controllers\Api\CallController;
use App\Http\Controllers\Api\CallReturnController;
use App\Http\Controllers\Api\CallSendProposalController;
use App\Http\Controllers\Api\CallSendProposalObservationController;
use App\Http\Controllers\Api\BusinessListController;
use App\Http\Controllers\Api\BusinessListAdditionalController;
use App\Http\Controllers\Api\BusinessListAdditionalPhonesController;
use App\Http\Controllers\Api\ClosedProposalConsultantController;
use App\Http\Controllers\Api\ClosedProposalController;
use App\Http\Controllers\Api\UserController;

use Illuminate\Support\Facades\Route;

Route::post('/login', [AuthController::class, 'auth']);
Route::post('/logout', [AuthController::class, 'logout'])->middleware('auth:sanctum');
Route::get('/me', [AuthController::class, 'me'])->middleware('auth:sanctum');

Route::middleware(['auth:sanctum'])->group(function () {
    Route::apiResource('/plans', PlanController::class); //Planos
});

Route::apiResource('/supports', SupportController::class);
Route::apiResource('/operators', OperatorController::class);//Operadoras
Route::apiResource('/status-implantations', StatusImplantationController::class);//Status Implatação
Route::apiResource('/service-status', ServiceStatusController::class); //AtendimentoStatus
Route::apiResource('/attend-send-proposal-status', AttendSendProposalStatusController::class); //atend_enviar_proposta_status
Route::apiResource('/type-shipments', AttendSendProposalTypeSendingController::class); //atend enviar proposta tipo envio

Route::apiResource('/calls', CallController::class); //Atendimentos
Route::apiResource('/call-returns', CallReturnController::class); //Atendimento Retornos
Route::apiResource('/call-send-proposals', CallSendProposalController::class);//Atendimento Enviar Proposta
Route::apiResource('/call-send-proposal-remarks', CallSendProposalObservationController::class);//Atendimento Enviar Proposta Observação
Route::apiResource('/business-lists', BusinessListController::class);//Lista Comercial
Route::apiResource('/business-list-complementary', BusinessListAdditionalController::class);//Lista Comercial Complementar
Route::apiResource('/business-list-compl-phones', BusinessListAdditionalPhonesController::class);//Lista Comercial Complementar Fones
Route::apiResource('/closed-proposal-consultants', ClosedProposalConsultantController::class);//Proposta Fechada Consultores
Route::apiResource('/closed-proposals', ClosedProposalController::class);//Proposta Fechada
Route::apiResource('/users', UserController::class);//Usuarios
