<?php

namespace App\Services;

use App\DTO\CallSendProposalRemarks\CreateCallSendProposalObservationDTO;
use App\DTO\CallSendProposalRemarks\UpdateCallSendProposalObservationDTO;


use App\Repositories\CallSendProposalObservationRepositoryInterface;
use App\Repositories\PaginationInterface;
use stdClass;

class CallSendProposalObservationService
{
    public function __construct(
        protected CallSendProposalObservationRepositoryInterface $repository,
    ) {}

    public function paginate(
        int $page = 1,
        int $totalPerPage = 15,
        string $filter = null
    ): PaginationInterface {
        return $this->repository->paginate(
            page: $page,
            totalPerPage: $totalPerPage,
            filter: $filter,
        );
    }

    public function getAll(string $filter = null): array
    {
        return $this->repository->getAll($filter);
    }

    public function findOne(string $id): stdClass|null
    {
        return $this->repository->findOne($id);
    }

    public function new(CreateCallSendProposalObservationDTO $dto): stdClass
    {
        return $this->repository->new($dto);
    }

    public function update(UpdateCallSendProposalObservationDTO $dto): stdClass|null
    {
        return $this->repository->update($dto);
    }

    public function delete(string $id): void
    {
        $this->repository->delete($id);
    }

}
