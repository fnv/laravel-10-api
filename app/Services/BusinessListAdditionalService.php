<?php

namespace App\Services;

use App\DTO\BusinessListsComplementary\CreateBusinessListAdditionalDTO;
use App\DTO\BusinessListsComplementary\UpdateBusinessListAdditionalDTO;

use App\Repositories\BusinessListAdditionalRepositoryInterface;
use App\Repositories\PaginationInterface;
use stdClass;

class BusinessListAdditionalService
{
    public function __construct(
        protected BusinessListAdditionalRepositoryInterface $repository,
    ) {}

    public function paginate(
        int $page = 1,
        int $totalPerPage = 15,
        string $filter = null
    ): PaginationInterface {
        return $this->repository->paginate(
            page: $page,
            totalPerPage: $totalPerPage,
            filter: $filter,
        );
    }

    public function getAll(string $filter = null): array
    {
        return $this->repository->getAll($filter);
    }

    public function findOne(string $id): stdClass|null
    {
        return $this->repository->findOne($id);
    }

    public function new(CreateBusinessListAdditionalDTO $dto): stdClass
    {
        return $this->repository->new($dto);
    }

    public function update(UpdateBusinessListAdditionalDTO $dto): stdClass|null
    {
        return $this->repository->update($dto);
    }

    public function delete(string $id): void
    {
        $this->repository->delete($id);
    }

}
