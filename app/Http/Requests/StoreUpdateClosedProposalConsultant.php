<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUpdateClosedProposalConsultant extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $rules = [
            'status_implantacao_id' => 'required',
            'plano_id' => 'required',
            'atend_enviar_proposta_id' => 'required',
            'data_fechamento' => 'required',
        ];

        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['status_implantacao_id'] = ['required'];
            $rules['plano_id'] = ['required'];
            $rules['atend_enviar_proposta_id'] = ['required'];
            $rules['data_fechamento'] = ['required'];
        }

        return $rules;
    }
}
