<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUpdateCallSendProposal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $rules = [
            'atendimento_id' => 'required',
            'atend_enviar_proposta_tipo_envio_id' => 'required',
            'atend_enviar_proposta_status_id' => 'required',
            'nome' => 'required',
            'cpf' => 'required',
            'proposta_anexo' => 'required',
            'data_envio' => 'required',
        ];

        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['atendimento_id'] = ['required'];
            $rules['atend_enviar_proposta_tipo_envio_id'] = ['required'];
            $rules['atend_enviar_proposta_status_id'] = ['required'];
            $rules['nome'] = ['required'];
            $rules['cpf'] = ['required'];
            $rules['proposta_anexo'] = ['required'];
            $rules['data_envio'] = ['required'];
        }

        return $rules;
    }
}
