<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUpdateBusinessListAdditional extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $rules = [
            'lista_comercial_id' => 'required',//não deixa duplicar nome na tabela (unique:operadoras)
            'cidade_id' => 'required',
            'quantidade_cnpj_no_cpf' => 'required',
            'email' => 'required',
            'idade' => 'required',
        ];

        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['lista_comercial_id'] = ['required'];
            $rules['cidade_id'] = ['required'];
            $rules['quantidade_cnpj_no_cpf'] = ['required'];
            $rules['email'] = ['required'];
            $rules['idade'] = ['required'];
        }

        return $rules;
    }
}
