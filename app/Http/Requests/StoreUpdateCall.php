<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUpdateCall extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $rules = [
            'lista_comercial_id' => 'required',
            'user_id' => 'required',
            'atendimento_status_id' => 'required',
        ];

        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['lista_comercial_id'] = ['required'];
            $rules['user_id'] = ['required'];
            $rules['atendimento_status_id'] = ['required'];
        }

        return $rules;
    }
}
