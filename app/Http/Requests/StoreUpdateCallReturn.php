<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUpdateCallReturn extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $rules = [
            'atendimento_id' => 'required',
            'cpf' => 'required',
            'nome' => 'required',
            'data' => 'required',
            'hora' => 'required',
            'observacao' => 'required',
        ];

        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['atendimento_id'] = ['required'];
            $rules['cpf'] = ['required'];
            $rules['nome'] = ['required'];
            $rules['data'] = ['required'];
            $rules['hora'] = ['required'];
            $rules['observacao'] = ['required'];
        }

        return $rules;
    }
}
