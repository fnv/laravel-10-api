<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUpdateBusinessListAdditionalPhones extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $rules = [
            'lista_comercial_complementar_id' => 'required',//não deixa duplicar nome na tabela (unique:operadoras)
            'telefone' => 'required',
        ];

        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['lista_comercial_complementar_id'] = ['required'];
            $rules['telefone'] = ['required'];
        }

        return $rules;
    }
}
