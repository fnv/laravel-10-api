<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUpdateServiceStatus extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $rules = [
            'nome' => 'required|min:3|max:255|unique:atendimento_status',//não deixa duplicar nome na tabela (unique:operadoras)
        ];

        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['nome'] = [
                'required', // 'nullable',
                'min:3',
                'max:255',
                // "unique:supports,subject,{$this->id},id",
                Rule::unique('atendimento_status')->ignore($this->serviceStatus ?? $this->id),
            ];
        }

        return $rules;
    }
}
