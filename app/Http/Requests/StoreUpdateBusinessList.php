<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUpdateBusinessList extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $rules = [
            'cpf' => 'required|min:11|max:11|unique:lista_comercial',//não deixa duplicar nome na tabela (unique:operadoras)
            'nome' => 'required|min:3|max:255',
            'razao_social' => 'required',
            'plano' => 'required',
            'data_de_vigencia' => 'required',
            'valor' => 'required',
            'quantidade_vidas' => 'required',
            'descricao' => 'required',
        ];

        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['cpf'] = [
                'required', // 'nullable',
                'min:11',
                'max:11',
                // "unique:supports,subject,{$this->id},id",
                Rule::unique('lista_comercial')->ignore($this->operator ?? $this->id),
            ];
            $rules['nome'] = [
                'required', // 'nullable',
                'min:3',
                'max:255'
            ];
            $rules['razao_social'] = ['required'];
            $rules['plano'] = ['required'];
            $rules['data_de_vigencia'] = ['required'];
            $rules['valor'] = ['required'];
            $rules['quantidade_vidas'] = ['required'];
            $rules['descricao'] = ['required'];
        }

        return $rules;
    }
}
