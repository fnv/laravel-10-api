<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUpdatePlan extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $rules = [
            'operadora_id' => 'required',
            'nome' => 'required|min:3|max:255|unique:planos',//não deixa duplicar nome na tabela (unique:operadoras)
            'valor' => 'required',
        ];


        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['operadora_id'] = ['required'];
            $rules['nome'] = [
                'required', // 'nullable',
                'min:3',
                'max:255',
                // "unique:supports,subject,{$this->id},id",
                Rule::unique('planos')->ignore($this->plan ?? $this->id),
            ];
            $rules['valor'] = ['required'];
        }

        return $rules;
    }
}
