<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUpdateClosedProposal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $rules = [
            'proposta_fechada_consultore_id' => 'required',
            'consultor_id_que_fechou' => 'required',
            'razao_social' => 'required',
            'cnpj' => 'required',
            'cnpj_cartao_cnpj_anexo' => 'required',
            'email' => 'required',
            'vigencia' => 'required',
            'carta_permanencia_anexo' => 'required',
            'cod_porte' => 'required',
            'telefone' => 'required',
            'tipo_de_tabela_cotada_anexo' => 'required',
            'valor_total_sem_iof' => 'required',
        ];

        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['proposta_fechada_consultore_id'] = ['required'];
            $rules['consultor_id_que_fechou'] = ['required'];
            $rules['razao_social'] = ['required'];
            $rules['cnpj'] = ['required'];
            $rules['cnpj_cartao_cnpj_anexo'] = ['required'];
            $rules['email'] = ['required'];
            $rules['vigencia'] = ['required'];
            $rules['carta_permanencia_anexo'] = ['required'];
            $rules['cod_porte'] = ['required'];
            $rules['telefone'] = ['required'];
            $rules['tipo_de_tabela_cotada_anexo'] = ['required'];
            $rules['valor_total_sem_iof'] = ['required'];
        }

        return $rules;
    }
}
