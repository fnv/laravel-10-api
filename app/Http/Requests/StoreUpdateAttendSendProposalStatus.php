<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUpdateAttendSendProposalStatus extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $rules = [
            'nome' => 'required|min:3|max:255|unique:atend_enviar_proposta_statuses',//não deixa duplicar nome na tabela (unique:operadoras)
        ];

        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['nome'] = [
                'required', // 'nullable',
                'min:3',
                'max:255',
                // "unique:supports,subject,{$this->id},id",
                Rule::unique('atend_enviar_proposta_statuses')->ignore($this->attendSendProposalStatus ?? $this->id),
            ];
        }

        return $rules;
    }
}
