<?php

namespace App\Http\Controllers\Api;

use App\Adapters\ApiAdapter;
use App\DTO\CallSendProposalRemarks\CreateCallSendProposalObservationDTO;
use App\DTO\CallSendProposalRemarks\UpdateCallSendProposalObservationDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateCallSendProposalObservation;
use App\Http\Resources\CallSendProposalObservationResource;
use App\Services\CallSendProposalObservationService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CallSendProposalObservationController extends Controller
{
    public function __construct(
        protected CallSendProposalObservationService $service,
    ) {}

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $operators = $this->service->paginate(
            page: $request->get('page', 1),
            totalPerPage: $request->get('per_page', 15),
            filter: $request->filter,
        );

        return ApiAdapter::toJson($operators);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUpdateCallSendProposalObservation $request)
    {
        $operator = $this->service->new(
            CreateCallSendProposalObservationDTO::makeFromRequest($request)
        );

        return new CallSendProposalObservationResource($operator);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (!$operator = $this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new CallSendProposalObservationResource($operator);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreUpdateCallSendProposalObservation $request, string $id)
    {
        $operator = $this->service->update(
            UpdateCallSendProposalObservationDTO::makeFromRequest($request, $id)
        );

        if (!$operator) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new CallSendProposalObservationResource($operator);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!$this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        $this->service->delete($id);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
