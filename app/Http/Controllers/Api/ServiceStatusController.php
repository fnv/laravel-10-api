<?php

namespace App\Http\Controllers\Api;

use App\Adapters\ApiAdapter;
use App\DTO\AssistanceStatus\CreateServiceStatusDTO;
use App\DTO\AssistanceStatus\UpdateServiceStatusDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateServiceStatus;
use App\Http\Resources\ServiceStatusResource;
use App\Services\ServiceStatusService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ServiceStatusController extends Controller
{
    public function __construct(
        protected ServiceStatusService $service,
    ) {}

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $results = $this->service->paginate(
            page: $request->get('page', 1),
            totalPerPage: $request->get('per_page', 15),
            filter: $request->filter,
        );

        return ApiAdapter::toJson($results);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUpdateServiceStatus $request)
    {
        $result = $this->service->new(
            CreateServiceStatusDTO::makeFromRequest($request)
        );

        return new ServiceStatusResource($result);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (!$result = $this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new ServiceStatusResource($result);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreUpdateServiceStatus $request, string $id)
    {
        $result = $this->service->update(
            UpdateServiceStatusDTO::makeFromRequest($request, $id)
        );

        if (!$result) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new ServiceStatusResource($result);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!$this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        $this->service->delete($id);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
