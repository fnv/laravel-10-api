<?php

namespace App\Http\Controllers\Api;

use App\Adapters\ApiAdapter;
use App\DTO\CallReturns\CreateCallReturnDTO;
use App\DTO\CallReturns\UpdateCallReturnDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateCallReturn;
use App\Http\Resources\CallReturnResource;
use App\Services\CallReturnService;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CallReturnController extends Controller
{
    public function __construct(
        protected CallReturnService $service,
    ) {}

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $operators = $this->service->paginate(
            page: $request->get('page', 1),
            totalPerPage: $request->get('per_page', 15),
            filter: $request->filter,
        );

        return ApiAdapter::toJson($operators);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUpdateCallReturn $request)
    {
        $operator = $this->service->new(
            CreateCallReturnDTO::makeFromRequest($request)
        );

        return new CallReturnResource($operator);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (!$operator = $this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new CallReturnResource($operator);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreUpdateCallReturn $request, string $id)
    {
        $operator = $this->service->update(
            UpdateCallReturnDTO::makeFromRequest($request, $id)
        );

        if (!$operator) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new CallReturnResource($operator);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!$this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        $this->service->delete($id);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
