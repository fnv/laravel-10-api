<?php

namespace App\Http\Controllers\Api;

use App\Adapters\ApiAdapter;
use App\DTO\Calls\CreateCallDTO;
use App\DTO\Calls\UpdateCallDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateCall;
use App\Http\Resources\CallResource;
use App\Services\CallService;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CallController extends Controller
{
    public function __construct(
        protected CallService $service,
    ) {}

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $operators = $this->service->paginate(
            page: $request->get('page', 1),
            totalPerPage: $request->get('per_page', 15),
            filter: $request->filter,
        );

        return ApiAdapter::toJson($operators);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUpdateCall $request)
    {
        $operator = $this->service->new(
            CreateCallDTO::makeFromRequest($request)
        );

        return new CallResource($operator);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (!$operator = $this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new CallResource($operator);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreUpdateCall $request, string $id)
    {
        $operator = $this->service->update(
            UpdateCallDTO::makeFromRequest($request, $id)
        );

        if (!$operator) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new CallResource($operator);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!$this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        $this->service->delete($id);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
