<?php

namespace App\Http\Controllers\Api;

use App\Adapters\ApiAdapter;
use App\DTO\ClosedProposals\CreateClosedProposalDTO;
use App\DTO\ClosedProposals\UpdateClosedProposalDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateClosedProposal;
use App\Http\Resources\ClosedProposalResource;
use App\Services\ClosedProposalService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ClosedProposalController extends Controller
{
    public function __construct(
        protected ClosedProposalService $service,
    ) {}

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $operators = $this->service->paginate(
            page: $request->get('page', 1),
            totalPerPage: $request->get('per_page', 15),
            filter: $request->filter,
        );

        return ApiAdapter::toJson($operators);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUpdateClosedProposal $request)
    {
        $operator = $this->service->new(
            CreateClosedProposalDTO::makeFromRequest($request)
        );

        return new ClosedProposalResource($operator);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (!$operator = $this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new ClosedProposalResource($operator);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreUpdateClosedProposal $request, string $id)
    {
        $operator = $this->service->update(
            UpdateClosedProposalDTO::makeFromRequest($request, $id)
        );

        if (!$operator) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new ClosedProposalResource($operator);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!$this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        $this->service->delete($id);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
