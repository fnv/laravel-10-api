<?php

namespace App\Http\Controllers\Api;

use App\Adapters\ApiAdapter;
use App\DTO\AttendSendProposalTypeSubmissions\CreateAttendSendProposalTypeSendingDTO;
use App\DTO\AttendSendProposalTypeSubmissions\UpdateAttendSendProposalTypeSendingDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateAttendSendProposalTypeSending;
use App\Http\Resources\AttendSendProposalTypeSendingResource;
use App\Services\AttendSendProposalTypeSendingService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AttendSendProposalTypeSendingController extends Controller
{
    public function __construct(
        protected AttendSendProposalTypeSendingService $service,
    ) {}

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $operators = $this->service->paginate(
            page: $request->get('page', 1),
            totalPerPage: $request->get('per_page', 15),
            filter: $request->filter,
        );

        return ApiAdapter::toJson($operators);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUpdateAttendSendProposalTypeSending $request)
    {
        $operator = $this->service->new(
            CreateAttendSendProposalTypeSendingDTO::makeFromRequest($request)
        );

        return new AttendSendProposalTypeSendingResource($operator);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (!$operator = $this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new AttendSendProposalTypeSendingResource($operator);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreUpdateAttendSendProposalTypeSending $request, string $id)
    {
        $operator = $this->service->update(
            UpdateAttendSendProposalTypeSendingDTO::makeFromRequest($request, $id)
        );

        if (!$operator) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new AttendSendProposalTypeSendingResource($operator);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!$this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        $this->service->delete($id);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
