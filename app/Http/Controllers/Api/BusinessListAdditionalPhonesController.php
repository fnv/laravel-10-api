<?php

namespace App\Http\Controllers\Api;

use App\Adapters\ApiAdapter;
use App\DTO\BusinessListsComplementaryPhones\CreateBusinessListAdditionalPhonesDTO;
use App\DTO\BusinessListsComplementaryPhones\UpdateBusinessListAdditionalPhonesDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateBusinessListAdditionalPhones;
use App\Http\Resources\BusinessListAdditionalPhonesResource;
use App\Services\BusinessListAdditionalPhonesService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BusinessListAdditionalPhonesController extends Controller
{
    public function __construct(
        protected BusinessListAdditionalPhonesService $service,
    ) {}

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $operators = $this->service->paginate(
            page: $request->get('page', 1),
            totalPerPage: $request->get('per_page', 15),
            filter: $request->filter,
        );

        return ApiAdapter::toJson($operators);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUpdateBusinessListAdditionalPhones $request)
    {
        $operator = $this->service->new(
            CreateBusinessListAdditionalPhonesDTO::makeFromRequest($request)
        );

        return new BusinessListAdditionalPhonesResource($operator);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (!$operator = $this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new BusinessListAdditionalPhonesResource($operator);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreUpdateBusinessListAdditionalPhones $request, string $id)
    {
        $operator = $this->service->update(
            UpdateBusinessListAdditionalPhonesDTO::makeFromRequest($request, $id)
        );

        if (!$operator) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new BusinessListAdditionalPhonesResource($operator);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!$this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        $this->service->delete($id);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
