<?php

namespace App\Http\Controllers\Api;

use App\Adapters\ApiAdapter;
use App\DTO\AttendSendProposalStatuses\CreateAttendSendProposalStatusDTO;
use App\DTO\AttendSendProposalStatuses\UpdateAttendSendProposalStatusDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateAttendSendProposalStatus;
use App\Http\Resources\AttendSendProposalStatusResource;
use App\Services\AttendSendProposalStatusService;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AttendSendProposalStatusController extends Controller
{
    public function __construct(
        protected AttendSendProposalStatusService $service,
    ) {}

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $results = $this->service->paginate(
            page: $request->get('page', 1),
            totalPerPage: $request->get('per_page', 15),
            filter: $request->filter,
        );

        return ApiAdapter::toJson($results);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUpdateAttendSendProposalStatus $request)
    {
        $result = $this->service->new(
            CreateAttendSendProposalStatusDTO::makeFromRequest($request)
        );

        return new AttendSendProposalStatusResource($result);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (!$result = $this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new AttendSendProposalStatusResource($result);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreUpdateAttendSendProposalStatus $request, string $id)
    {
        $result = $this->service->update(
            UpdateAttendSendProposalStatusDTO::makeFromRequest($request, $id)
        );

        if (!$result) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new AttendSendProposalStatusResource($result);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!$this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        $this->service->delete($id);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
