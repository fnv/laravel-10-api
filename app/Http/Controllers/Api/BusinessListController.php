<?php

namespace App\Http\Controllers\Api;

use App\Adapters\ApiAdapter;
use App\DTO\BusinessLists\CreateBusinessListDTO;
use App\DTO\BusinessLists\UpdateBusinessListDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateBusinessList;
use App\Http\Resources\BusinessListResource;
use App\Services\BusinessListService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BusinessListController extends Controller
{
    public function __construct(
        protected BusinessListService $service,
    ) {}

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $operators = $this->service->paginate(
            page: $request->get('page', 1),
            totalPerPage: $request->get('per_page', 15),
            filter: $request->filter,
        );

        return ApiAdapter::toJson($operators);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUpdateBusinessList $request)
    {
        $operator = $this->service->new(
            CreateBusinessListDTO::makeFromRequest($request)
        );

        return new BusinessListResource($operator);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (!$operator = $this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new BusinessListResource($operator);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreUpdateBusinessList $request, string $id)
    {
        $operator = $this->service->update(
            UpdateBusinessListDTO::makeFromRequest($request, $id)
        );

        if (!$operator) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new BusinessListResource($operator);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!$this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        $this->service->delete($id);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
