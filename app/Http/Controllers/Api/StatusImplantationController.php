<?php

namespace App\Http\Controllers\Api;

use App\Adapters\ApiAdapter;
use App\DTO\StatusImplantations\CreateStatusImplantationDTO;
use App\DTO\StatusImplantations\UpdateStatusImplantationDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateStatusImplantation;
use App\Http\Resources\StatusImplantationResource;
use App\Services\StatusImplantationService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StatusImplantationController extends Controller
{
    public function __construct(
        protected StatusImplantationService $service,
    ) {}

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $results = $this->service->paginate(
            page: $request->get('page', 1),
            totalPerPage: $request->get('per_page', 15),
            filter: $request->filter,
        );

        return ApiAdapter::toJson($results);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUpdateStatusImplantation $request)
    {
        $result = $this->service->new(
            CreateStatusImplantationDTO::makeFromRequest($request)
        );

        return new StatusImplantationResource($result);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (!$result = $this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new StatusImplantationResource($result);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreUpdateStatusImplantation $request, string $id)
    {
        $result = $this->service->update(
            UpdateStatusImplantationDTO::makeFromRequest($request, $id)
        );

        if (!$result) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new StatusImplantationResource($result);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!$this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        $this->service->delete($id);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
