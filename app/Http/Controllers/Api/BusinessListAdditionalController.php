<?php

namespace App\Http\Controllers\Api;

use App\Adapters\ApiAdapter;
use App\DTO\BusinessListsComplementary\CreateBusinessListAdditionalDTO;
use App\DTO\BusinessListsComplementary\UpdateBusinessListAdditionalDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateBusinessListAdditional;
use App\Http\Resources\BusinessListAdditionalResource;
use App\Services\BusinessListAdditionalService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BusinessListAdditionalController extends Controller
{
    public function __construct(
        protected BusinessListAdditionalService $service,
    ) {}

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $operators = $this->service->paginate(
            page: $request->get('page', 1),
            totalPerPage: $request->get('per_page', 15),
            filter: $request->filter,
        );

        return ApiAdapter::toJson($operators);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUpdateBusinessListAdditional $request)
    {
        $operator = $this->service->new(
            CreateBusinessListAdditionalDTO::makeFromRequest($request)
        );

        return new BusinessListAdditionalResource($operator);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (!$operator = $this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new BusinessListAdditionalResource($operator);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreUpdateBusinessListAdditional $request, string $id)
    {
        $operator = $this->service->update(
            UpdateBusinessListAdditionalDTO::makeFromRequest($request, $id)
        );

        if (!$operator) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new BusinessListAdditionalResource($operator);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!$this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        $this->service->delete($id);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
