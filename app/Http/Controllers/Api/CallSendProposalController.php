<?php

namespace App\Http\Controllers\Api;

use App\Adapters\ApiAdapter;
use App\DTO\CallSendProposals\CreateCallSendProposalDTO;
use App\DTO\CallSendProposals\UpdateCallSendProposalDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateCallSendProposal;
use App\Http\Resources\CallSendProposalResource;
use App\Services\CallSendProposalService;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CallSendProposalController extends Controller
{
    public function __construct(
        protected CallSendProposalService $service,
    ) {}

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $operators = $this->service->paginate(
            page: $request->get('page', 1),
            totalPerPage: $request->get('per_page', 15),
            filter: $request->filter,
        );

        return ApiAdapter::toJson($operators);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUpdateCallSendProposal $request)
    {
        $operator = $this->service->new(
            CreateCallSendProposalDTO::makeFromRequest($request)
        );

        return new CallSendProposalResource($operator);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (!$operator = $this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new CallSendProposalResource($operator);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreUpdateCallSendProposal $request, string $id)
    {
        $operator = $this->service->update(
            UpdateCallSendProposalDTO::makeFromRequest($request, $id)
        );

        if (!$operator) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new CallSendProposalResource($operator);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!$this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        $this->service->delete($id);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
