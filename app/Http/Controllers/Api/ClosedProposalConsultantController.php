<?php

namespace App\Http\Controllers\Api;

use App\Adapters\ApiAdapter;
use App\DTO\ClosedProposalConsultants\CreateClosedProposalConsultantDTO;
use App\DTO\ClosedProposalConsultants\UpdateClosedProposalConsultantDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateClosedProposalConsultant;
use App\Http\Resources\ClosedProposalConsultantResource;
use App\Services\ClosedProposalConsultantService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ClosedProposalConsultantController extends Controller
{
    public function __construct(
        protected ClosedProposalConsultantService $service,
    ) {}

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $operators = $this->service->paginate(
            page: $request->get('page', 1),
            totalPerPage: $request->get('per_page', 15),
            filter: $request->filter,
        );

        return ApiAdapter::toJson($operators);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUpdateClosedProposalConsultant $request)
    {
        $operator = $this->service->new(
            CreateClosedProposalConsultantDTO::makeFromRequest($request)
        );

        return new ClosedProposalConsultantResource($operator);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (!$operator = $this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new ClosedProposalConsultantResource($operator);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreUpdateClosedProposalConsultant $request, string $id)
    {
        $operator = $this->service->update(
            UpdateClosedProposalConsultantDTO::makeFromRequest($request, $id)
        );

        if (!$operator) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        return new ClosedProposalConsultantResource($operator);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!$this->service->findOne($id)) {
            return response()->json([
                'error' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
        }

        $this->service->delete($id);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
