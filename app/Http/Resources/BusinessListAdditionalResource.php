<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BusinessListAdditionalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'lista_comercial_id' => $this->lista_comercial_id,
            'cidade_id' => $this->cidade_id,
            'quantidade_cnpj_no_cpf' => $this->quantidade_cnpj_no_cpf,
            'email' => $this->email,
            'idade' => $this->idade,
            'dt_created' => Carbon::make($this->created_at)->format('Y-m-d'),
        ];
    }
}
