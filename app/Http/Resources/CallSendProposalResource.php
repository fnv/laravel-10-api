<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CallSendProposalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'atendimento_id' => $this->atendimento_id,
            'atend_enviar_proposta_tipo_envio_id' => $this->atend_enviar_proposta_tipo_envio_id,
            'atend_enviar_proposta_status_id' => $this->atend_enviar_proposta_status_id,
            'nome' => $this->nome,
            'cpf' => $this->cpf,
            'proposta_anexo' => $this->proposta_anexo,
            'data_envio' => $this->data_envio,
            'dt_created' => Carbon::make($this->created_at)->format('Y-m-d'),
        ];
    }
}
