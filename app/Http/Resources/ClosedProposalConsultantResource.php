<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ClosedProposalConsultantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'status_implantacao_id' => $this->status_implantacao_id,
            'plano_id' => $this->plano_id,
            'atend_enviar_proposta_id' => $this->atend_enviar_proposta_id,
            'data_fechamento' => $this->data_fechamento,
            'dt_created' => Carbon::make($this->created_at)->format('Y-m-d'),
        ];
    }
}
