<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BusinessListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'cpf' => $this->cpf,
            'nome' => $this->nome,
            'razao_social' => $this->razao_social,
            'data_de_vigencia' => $this->data_de_vigencia,
            'valor' => $this->valor,
            'quantidade_vidas' => $this->quantidade_vidas,
            'descricao' => $this->descricao,
            'dt_created' => Carbon::make($this->created_at)->format('Y-m-d'),
        ];
    }
}
