<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BusinessListAdditionalPhonesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'lista_comercial_complementar_id' => $this->lista_comercial_complementar_id,
            'telefone' => $this->telefone,
            'dt_created' => Carbon::make($this->created_at)->format('Y-m-d'),
        ];
    }
}
