<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'operadora_id' => $this->operadora_id,
            'nome' => $this->nome,
            'valor' => $this->valor,
            'dt_created' => Carbon::make($this->created_at)->format('Y-m-d'),
        ];
    }
}
