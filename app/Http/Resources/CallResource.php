<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CallResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'lista_comercial_id' => $this->user_id,
            'user_id' => $this->user_id,
            'atendimento_status_id' => $this->atendimento_status_id,
            'dt_created' => Carbon::make($this->created_at)->format('Y-m-d'),
        ];
    }
}
