<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CallReturnResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'atendimento_id' => $this->atendimento_id,
            'cpf' => $this->cpf,
            'nome' => $this->nome,
            'data' => $this->data,
            'hora' => $this->hora,
            'observacao' => $this->observacao,
            'dt_created' => Carbon::make($this->created_at)->format('Y-m-d'),
        ];
    }
}
