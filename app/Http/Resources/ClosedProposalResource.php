<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ClosedProposalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'proposta_fechada_consultore_id' => $this->proposta_fechada_consultore_id,
            'consultor_id_que_fechou' => $this->consultor_id_que_fechou,
            'razao_social' => $this->razao_social,
            'cnpj' => $this->cnpj,
            'cnpj_cartao_cnpj_anexo' => $this->cnpj_cartao_cnpj_anexo,
            'email' => $this->email,
            'vigencia' => $this->vigencia,
            'carta_permanencia_anexo' => $this->carta_permanencia_anexo,
            'cod_porte' => $this->cod_porte,
            'telefone' => $this->telefone,
            'tipo_de_tabela_cotada_anexo' => $this->tipo_de_tabela_cotada_anexo,
            'valor_total_sem_iof' => $this->valor_total_sem_iof,
            'dt_created' => Carbon::make($this->created_at)->format('Y-m-d'),
        ];
    }
}
