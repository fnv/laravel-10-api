<?php

namespace App\Repositories;

use App\DTO\ClosedProposalConsultants\CreateClosedProposalConsultantDTO;
use App\DTO\ClosedProposalConsultants\UpdateClosedProposalConsultantDTO;
use stdClass;

interface ClosedProposalConsultantRepositoryInterface
{
    public function paginate(int $page = 1, int $totalPerPage = 15, string $filter = null): PaginationInterface;
    public function getAll(string $filter = null): array;
    public function findOne(string $id): stdClass|null;
    public function delete(string $id): void;
    public function new(CreateClosedProposalConsultantDTO $dto): stdClass;
    public function update(UpdateClosedProposalConsultantDTO $dto): stdClass|null;
}
