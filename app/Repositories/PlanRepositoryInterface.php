<?php

namespace App\Repositories;

use App\DTO\Plans\CreatePlanDTO;
use App\DTO\Plans\UpdatePlanDTO;

use stdClass;

interface PlanRepositoryInterface
{
    public function paginate(int $page = 1, int $totalPerPage = 15, string $filter = null): PaginationInterface;
    public function getAll(string $filter = null): array;
    public function findOne(string $id): stdClass|null;
    public function delete(string $id): void;
    public function new(CreatePlanDTO $dto): stdClass;
    public function update(UpdatePlanDTO $dto): stdClass|null;
}
