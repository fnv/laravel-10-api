<?php

namespace App\Repositories;

use App\DTO\CallReturns\CreateCallReturnDTO;
use App\DTO\CallReturns\UpdateCallReturnDTO;
use stdClass;

interface CallReturnRepositoryInterface
{
    public function paginate(int $page = 1, int $totalPerPage = 15, string $filter = null): PaginationInterface;
    public function getAll(string $filter = null): array;
    public function findOne(string $id): stdClass|null;
    public function delete(string $id): void;
    public function new(CreateCallReturnDTO $dto): stdClass;
    public function update(UpdateCallReturnDTO $dto): stdClass|null;
}
