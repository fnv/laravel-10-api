<?php

namespace App\Repositories;

use App\DTO\BusinessListsComplementary\CreateBusinessListAdditionalDTO;
use App\DTO\BusinessListsComplementary\UpdateBusinessListAdditionalDTO;
use stdClass;

interface BusinessListAdditionalRepositoryInterface
{
    public function paginate(int $page = 1, int $totalPerPage = 15, string $filter = null): PaginationInterface;
    public function getAll(string $filter = null): array;
    public function findOne(string $id): stdClass|null;
    public function delete(string $id): void;
    public function new(CreateBusinessListAdditionalDTO $dto): stdClass;
    public function update(UpdateBusinessListAdditionalDTO $dto): stdClass|null;
}
