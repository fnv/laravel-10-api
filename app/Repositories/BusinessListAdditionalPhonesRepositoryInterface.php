<?php

namespace App\Repositories;

use App\DTO\BusinessListsComplementaryPhones\CreateBusinessListAdditionalPhonesDTO;
use App\DTO\BusinessListsComplementaryPhones\UpdateBusinessListAdditionalPhonesDTO;
use stdClass;

interface BusinessListAdditionalPhonesRepositoryInterface
{
    public function paginate(int $page = 1, int $totalPerPage = 15, string $filter = null): PaginationInterface;
    public function getAll(string $filter = null): array;
    public function findOne(string $id): stdClass|null;
    public function delete(string $id): void;
    public function new(CreateBusinessListAdditionalPhonesDTO $dto): stdClass;
    public function update(UpdateBusinessListAdditionalPhonesDTO $dto): stdClass|null;
}
