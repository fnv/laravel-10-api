<?php

namespace App\Repositories;

use App\DTO\BusinessLists\CreateBusinessListDTO;
use App\DTO\BusinessLists\UpdateBusinessListDTO;
use stdClass;

interface BusinessListRepositoryInterface
{
    public function paginate(int $page = 1, int $totalPerPage = 15, string $filter = null): PaginationInterface;
    public function getAll(string $filter = null): array;
    public function findOne(string $id): stdClass|null;
    public function delete(string $id): void;
    public function new(CreateBusinessListDTO $dto): stdClass;
    public function update(UpdateBusinessListDTO $dto): stdClass|null;
}
