<?php

namespace App\Repositories;

use App\DTO\AttendSendProposalStatuses\CreateAttendSendProposalStatusDTO;
use App\DTO\AttendSendProposalStatuses\UpdateAttendSendProposalStatusDTO;

use stdClass;

interface AttendSendProposalStatusRepositoryInterface
{
    public function paginate(int $page = 1, int $totalPerPage = 15, string $filter = null): PaginationInterface;
    public function getAll(string $filter = null): array;
    public function findOne(string $id): stdClass|null;
    public function delete(string $id): void;
    public function new(CreateAttendSendProposalStatusDTO $dto): stdClass;
    public function update(UpdateAttendSendProposalStatusDTO $dto): stdClass|null;
}
