<?php

namespace App\Repositories;

use App\DTO\AssistanceStatus\CreateServiceStatusDTO;
use App\DTO\AssistanceStatus\UpdateServiceStatusDTO;
use stdClass;

interface ServiceStatusRepositoryInterface
{
    public function paginate(int $page = 1, int $totalPerPage = 15, string $filter = null): PaginationInterface;
    public function getAll(string $filter = null): array;
    public function findOne(string $id): stdClass|null;
    public function delete(string $id): void;
    public function new(CreateServiceStatusDTO $dto): stdClass;
    public function update(UpdateServiceStatusDTO $dto): stdClass|null;
}
