<?php

namespace App\Repositories;

use App\DTO\AttendSendProposalTypeSubmissions\CreateAttendSendProposalTypeSendingDTO;
use App\DTO\AttendSendProposalTypeSubmissions\UpdateAttendSendProposalTypeSendingDTO;
use stdClass;

interface AttendSendProposalTypeSendingRepositoryInterface
{
    public function paginate(int $page = 1, int $totalPerPage = 15, string $filter = null): PaginationInterface;
    public function getAll(string $filter = null): array;
    public function findOne(string $id): stdClass|null;
    public function delete(string $id): void;
    public function new(CreateAttendSendProposalTypeSendingDTO $dto): stdClass;
    public function update(UpdateAttendSendProposalTypeSendingDTO $dto): stdClass|null;
}
