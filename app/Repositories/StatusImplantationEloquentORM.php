<?php

namespace App\Repositories;

use App\DTO\StatusImplantations\CreateStatusImplantationDTO;
use App\DTO\StatusImplantations\UpdateStatusImplantationDTO;
use App\Models\Api\StatusImplantation;
use App\Repositories\StatusImplantationRepositoryInterface;
use stdClass;

class StatusImplantationEloquentORM implements StatusImplantationRepositoryInterface
{
    public function __construct(
        protected StatusImplantation $model
    ) {}

    public function paginate(int $page = 1, int $totalPerPage = 15, string $filter = null): PaginationInterface
    {
        $result = $this->model
                       ->paginate($totalPerPage, ['*'], 'page', $page);

        return new PaginationPresenter($result);
    }

    public function getAll(string $filter = null): array
    {
        return $this->model->all()->toArray();

    }

    public function findOne(string $id): stdClass|null
    {
        $operator = $this->model->find($id);

        if (!$operator) {
            return null;
        }

        return (object) $operator->toArray();

    }
    public function delete(string $id): void
    {
        $this->model->findOrFail($id)->delete();
    }

    public function new(CreateStatusImplantationDTO $dto): stdClass
    {

        $operator = $this->model->create(
            (array) $dto
        );

        return (object) $operator->toArray();
    }

    public function update(UpdateStatusImplantationDTO $dto): stdClass|null
    {
        if (!$operator = $this->model->find($dto->id)) {
            return null;
        }

        $operator->update(
            (array) $dto
        );

        return (object) $operator->toArray();



    }
}
