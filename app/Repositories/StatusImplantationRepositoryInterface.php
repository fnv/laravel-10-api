<?php

namespace App\Repositories;

use App\DTO\StatusImplantations\CreateStatusImplantationDTO;
use App\DTO\StatusImplantations\UpdateStatusImplantationDTO;
use stdClass;

interface StatusImplantationRepositoryInterface
{
    public function paginate(int $page = 1, int $totalPerPage = 15, string $filter = null): PaginationInterface;
    public function getAll(string $filter = null): array;
    public function findOne(string $id): stdClass|null;
    public function delete(string $id): void;
    public function new(CreateStatusImplantationDTO $dto): stdClass;
    public function update(UpdateStatusImplantationDTO $dto): stdClass|null;
}
