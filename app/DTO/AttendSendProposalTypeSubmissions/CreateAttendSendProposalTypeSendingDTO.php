<?php

namespace App\DTO\AttendSendProposalTypeSubmissions;

use App\Http\Requests\StoreUpdateAttendSendProposalTypeSending;

class CreateAttendSendProposalTypeSendingDTO
{
    public function __construct(
        public string $nome,
    ) {}

    public static function makeFromRequest(StoreUpdateAttendSendProposalTypeSending $request): self
    {
        return new self(
            $request->nome,
        );
    }
}
