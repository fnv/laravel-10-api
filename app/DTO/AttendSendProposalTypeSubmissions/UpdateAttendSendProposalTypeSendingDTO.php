<?php

namespace App\DTO\AttendSendProposalTypeSubmissions;

use App\Http\Requests\StoreUpdateAttendSendProposalTypeSending;


class UpdateAttendSendProposalTypeSendingDTO
{
    public function __construct(
        public string $id,
        public string $nome,
    ) {}

    public static function makeFromRequest(StoreUpdateAttendSendProposalTypeSending $request, string $id = null): self
    {
        return new self(
            $id ?? $request->id,
            $request->nome,
        );
    }
}
