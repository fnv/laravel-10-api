<?php

namespace App\DTO\BusinessLists;

use App\Http\Requests\StoreUpdateBusinessList;

class UpdateBusinessListDTO
{
    public function __construct(
        public string $id,
        public string $cpf,
        public string $nome,
        public string $razao_social,
        public string $plano,
        public string $data_de_vigencia,
        public string $valor,
        public string $quantidade_vidas,
        public string $descricao,
    ) {}

    public static function makeFromRequest(StoreUpdateBusinessList $request, string $id = null): self
    {
        return new self(
            $id ?? $request->id,
            $request->cpf,
            $request->nome,
            $request->razao_social,
            $request->plano,
            $request->data_de_vigencia,
            $request->valor,
            $request->quantidade_vidas,
            $request->descricao,
        );
    }
}
