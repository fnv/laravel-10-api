<?php

namespace App\DTO\StatusImplantations;

use App\Http\Requests\StoreUpdateStatusImplantation;

class UpdateStatusImplantationDTO
{
    public function __construct(
        public string $id,
        public string $nome,
    ) {}

    public static function makeFromRequest(StoreUpdateStatusImplantation $request, string $id = null): self
    {
        return new self(
            $id ?? $request->id,
            $request->nome,
        );
    }
}
