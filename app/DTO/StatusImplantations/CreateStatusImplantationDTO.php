<?php

namespace App\DTO\StatusImplantations;

use App\Http\Requests\StoreUpdateStatusImplantation;

class CreateStatusImplantationDTO
{
    public function __construct(
        public string $nome,
    ) {}

    public static function makeFromRequest(StoreUpdateStatusImplantation $request): self
    {
        return new self(
            $request->nome,
        );
    }
}
