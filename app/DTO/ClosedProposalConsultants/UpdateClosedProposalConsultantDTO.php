<?php

namespace App\DTO\ClosedProposalConsultants;

use App\Http\Requests\StoreUpdateClosedProposalConsultant;

class UpdateClosedProposalConsultantDTO
{
    public function __construct(
        public string $id,
        public string $status_implantacao_id,
        public string $plano_id,
        public string $atend_enviar_proposta_id,
        public string $data_fechamento,
    ) {}

    public static function makeFromRequest(StoreUpdateClosedProposalConsultant $request, string $id = null): self
    {
        return new self(
            $id ?? $request->id,
            $request->status_implantacao_id,
            $request->plano_id,
            $request->atend_enviar_proposta_id,
            $request->data_fechamento,
        );
    }
}
