<?php

namespace App\DTO\ClosedProposalConsultants;

use App\Http\Requests\StoreUpdateClosedProposalConsultant;

class CreateClosedProposalConsultantDTO
{
    public function __construct(
        public string $status_implantacao_id,
        public string $plano_id,
        public string $atend_enviar_proposta_id,
        public string $data_fechamento,
    ) {}

    public static function makeFromRequest(StoreUpdateClosedProposalConsultant $request): self
    {
        return new self(
            $request->status_implantacao_id,
            $request->plano_id,
            $request->atend_enviar_proposta_id,
            $request->data_fechamento,
        );
    }
}
