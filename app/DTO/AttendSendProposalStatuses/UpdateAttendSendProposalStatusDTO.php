<?php

namespace App\DTO\AttendSendProposalStatuses;

use App\Http\Requests\StoreUpdateAttendSendProposalStatus;

class UpdateAttendSendProposalStatusDTO
{
    public function __construct(
        public string $id,
        public string $nome,
    ) {}

    public static function makeFromRequest(StoreUpdateAttendSendProposalStatus $request, string $id = null): self
    {
        return new self(
            $id ?? $request->id,
            $request->nome,
        );
    }
}
