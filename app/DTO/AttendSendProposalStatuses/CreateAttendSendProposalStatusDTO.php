<?php

namespace App\DTO\AttendSendProposalStatuses;

use App\Http\Requests\StoreUpdateAttendSendProposalStatus;

class CreateAttendSendProposalStatusDTO
{
    public function __construct(
        public string $nome,
    ) {}

    public static function makeFromRequest(StoreUpdateAttendSendProposalStatus $request): self
    {
        return new self(
            $request->nome,
        );
    }
}
