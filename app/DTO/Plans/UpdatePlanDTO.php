<?php

namespace App\DTO\Plans;

use App\Http\Requests\StoreUpdatePlan;
use Brick\Math\BigInteger;
use Ramsey\Uuid\Type\Decimal;

class UpdatePlanDTO
{
    public function __construct(
        public string $id,
        public string $operadora_id,
        public string $nome,
        public string $valor,
    ) {}

    public static function makeFromRequest(StoreUpdatePlan $request, string $id = null): self
    {
        return new self(
            $id ?? $request->id,
            $request->operadora_id,
            $request->nome,
            $request->valor
        );
    }
}
