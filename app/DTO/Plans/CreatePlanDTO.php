<?php

namespace App\DTO\Plans;

use App\Http\Requests\StoreUpdatePlan;
use Ramsey\Uuid\Type\Decimal;

class CreatePlanDTO
{
    public function __construct(
        public string $operadora_id,
        public string $nome,
        public string $valor,
    ) {}

    public static function makeFromRequest(StoreUpdatePlan $request): self
    {
        return new self(
            $request->operadora_id,
            $request->nome,
            $request->valor
        );
    }
}
