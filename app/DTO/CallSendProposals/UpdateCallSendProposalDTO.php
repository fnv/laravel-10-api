<?php

namespace App\DTO\CallSendProposals;

use App\Http\Requests\StoreUpdateCallSendProposal;

class UpdateCallSendProposalDTO
{
    public function __construct(
        public string $id,
        public string $atendimento_id,
        public string $atend_enviar_proposta_tipo_envio_id,
        public string $atend_enviar_proposta_status_id,
        public string $nome,
        public string $cpf,
        public string $proposta_anexo,
        public string $data_envio,
    ) {}

    public static function makeFromRequest(StoreUpdateCallSendProposal $request, string $id = null): self
    {
        return new self(
            $id ?? $request->id,
            $request->atendimento_id,
            $request->atend_enviar_proposta_tipo_envio_id,
            $request->atend_enviar_proposta_status_id,
            $request->nome,
            $request->cpf,
            $request->proposta_anexo,
            $request->data_envio,
        );
    }
}
