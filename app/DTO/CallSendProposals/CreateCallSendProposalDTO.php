<?php

namespace App\DTO\CallSendProposals;

use App\Http\Requests\StoreUpdateCallSendProposal;

class CreateCallSendProposalDTO
{
    public function __construct(
        public string $atendimento_id,
        public string $atend_enviar_proposta_tipo_envio_id,
        public string $atend_enviar_proposta_status_id,
        public string $nome,
        public string $cpf,
        public string $proposta_anexo,
        public string $data_envio,
    ) {}

    public static function makeFromRequest(StoreUpdateCallSendProposal $request): self
    {
        return new self(
            $request->atendimento_id,
            $request->atend_enviar_proposta_tipo_envio_id,
            $request->atend_enviar_proposta_status_id,
            $request->nome,
            $request->cpf,
            $request->proposta_anexo,
            $request->data_envio,
        );
    }
}
