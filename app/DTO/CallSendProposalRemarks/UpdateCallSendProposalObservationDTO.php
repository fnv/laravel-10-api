<?php

namespace App\DTO\CallSendProposalRemarks;

use App\Http\Requests\StoreUpdateCallSendProposalObservation;

class UpdateCallSendProposalObservationDTO
{
    public function __construct(
        public string $id,
        public string $atend_enviar_proposta_id,
        public string $observacao,
    ) {}

    public static function makeFromRequest(StoreUpdateCallSendProposalObservation $request, string $id = null): self
    {
        return new self(
            $id ?? $request->id,
            $request->atend_enviar_proposta_id,
            $request->observacao,
        );
    }
}
