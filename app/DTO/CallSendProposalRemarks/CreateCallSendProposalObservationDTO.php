<?php

namespace App\DTO\CallSendProposalRemarks;

use App\Http\Requests\StoreUpdateCallSendProposalObservation;

class CreateCallSendProposalObservationDTO
{
    public function __construct(
        public string $atend_enviar_proposta_id,
        public string $observacao,
    ) {}

    public static function makeFromRequest(StoreUpdateCallSendProposalObservation $request): self
    {
        return new self(
            $request->atend_enviar_proposta_id,
            $request->observacao,
        );
    }
}
