<?php

namespace App\DTO\CallReturns;

use App\Http\Requests\StoreUpdateCallReturn;

class CreateCallReturnDTO
{
    public function __construct(
        public string $atendimento_id,
        public string $cpf,
        public string $nome,
        public string $data,
        public string $hora,
        public string $observacao,
    ) {}

    public static function makeFromRequest(StoreUpdateCallReturn $request): self
    {
        return new self(
            $request->atendimento_id,
            $request->cpf,
            $request->nome,
            $request->data,
            $request->hora,
            $request->observacao,
        );
    }
}
