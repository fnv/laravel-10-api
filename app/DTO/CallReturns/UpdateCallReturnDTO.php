<?php

namespace App\DTO\CallReturns;

use App\Http\Requests\StoreUpdateCallReturn;

class UpdateCallReturnDTO
{
    public function __construct(
        public string $id,
        public string $atendimento_id,
        public string $cpf,
        public string $nome,
        public string $data,
        public string $hora,
        public string $observacao,
    ) {}

    public static function makeFromRequest(StoreUpdateCallReturn $request, string $id = null): self
    {
        return new self(
            $id ?? $request->id,
            $request->atendimento_id,
            $request->cpf,
            $request->nome,
            $request->data,
            $request->hora,
            $request->observacao,
        );
    }
}
