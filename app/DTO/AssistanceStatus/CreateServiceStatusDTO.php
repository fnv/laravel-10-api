<?php

namespace App\DTO\AssistanceStatus;

use App\Http\Requests\StoreUpdateServiceStatus;

class CreateServiceStatusDTO
{
    public function __construct(
        public string $nome,
    ) {}

    public static function makeFromRequest(StoreUpdateServiceStatus $request): self
    {
        return new self(
            $request->nome,
        );
    }
}
