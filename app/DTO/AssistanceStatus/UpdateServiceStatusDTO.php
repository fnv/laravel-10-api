<?php

namespace App\DTO\AssistanceStatus;

use App\Http\Requests\StoreUpdateServiceStatus;

class UpdateServiceStatusDTO
{
    public function __construct(
        public string $id,
        public string $nome,
    ) {}

    public static function makeFromRequest(StoreUpdateServiceStatus $request, string $id = null): self
    {
        return new self(
            $id ?? $request->id,
            $request->nome,
        );
    }
}
