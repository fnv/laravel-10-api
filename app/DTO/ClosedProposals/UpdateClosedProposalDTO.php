<?php

namespace App\DTO\ClosedProposals;

use App\Http\Requests\StoreUpdateClosedProposal;

class UpdateClosedProposalDTO
{
    public function __construct(
        public string $id,
        public string $proposta_fechada_consultore_id,
        public string $consultor_id_que_fechou,
        public string $razao_social,
        public string $cnpj,
        public string $cnpj_cartao_cnpj_anexo,
        public string $email,
        public string $vigencia,
        public string $carta_permanencia_anexo,
        public string $cod_porte,
        public string $telefone,
        public string $tipo_de_tabela_cotada_anexo,
        public string $valor_total_sem_iof,
    ) {}

    public static function makeFromRequest(StoreUpdateClosedProposal $request, string $id = null): self
    {
        return new self(
            $id ?? $request->id,
            $request->proposta_fechada_consultore_id,
            $request->consultor_id_que_fechou,
            $request->razao_social,
            $request->cnpj,
            $request->cnpj_cartao_cnpj_anexo,
            $request->email,
            $request->vigencia,
            $request->carta_permanencia_anexo,
            $request->cod_porte,
            $request->telefone,
            $request->tipo_de_tabela_cotada_anexo,
            $request->valor_total_sem_iof,
        );
    }
}
