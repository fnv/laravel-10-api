<?php

namespace App\DTO\Calls;

use App\Http\Requests\StoreUpdateCall;

class UpdateCallDTO
{
    public function __construct(
        public string $id,
        public string $lista_comercial_id,
        public string $user_id,
        public string $atendimento_status_id,
    ) {}

    public static function makeFromRequest(StoreUpdateCall $request, string $id = null): self
    {
        return new self(
            $id ?? $request->id,
            $request->lista_comercial_id,
            $request->user_id,
            $request->atendimento_status_id,
        );
    }
}
