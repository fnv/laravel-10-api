<?php

namespace App\DTO\Calls;

use App\Http\Requests\StoreUpdateCall;

class CreateCallDTO
{
    public function __construct(
        public string $lista_comercial_id,
        public string $user_id,
        public string $atendimento_status_id,
    ) {}

    public static function makeFromRequest(StoreUpdateCall $request): self
    {
        return new self(
            $request->lista_comercial_id,
            $request->user_id,
            $request->atendimento_status_id,
        );
    }
}
