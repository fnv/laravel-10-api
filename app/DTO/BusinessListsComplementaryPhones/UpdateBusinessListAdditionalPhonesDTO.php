<?php

namespace App\DTO\BusinessListsComplementaryPhones;

use App\Http\Requests\StoreUpdateBusinessListAdditionalPhones;

class UpdateBusinessListAdditionalPhonesDTO
{
    public function __construct(
        public string $id,
        public string $lista_comercial_complementar_id,
        public string $telefone,
    ) {}

    public static function makeFromRequest(StoreUpdateBusinessListAdditionalPhones $request, string $id = null): self
    {
        return new self(
            $id ?? $request->id,
            $request->lista_comercial_complementar_id,
            $request->telefone,
        );
    }
}
