<?php

namespace App\DTO\BusinessListsComplementaryPhones;

use App\Http\Requests\StoreUpdateBusinessListAdditionalPhones;

class CreateBusinessListAdditionalPhonesDTO
{
    public function __construct(
        public string $lista_comercial_complementar_id,
        public string $telefone,
    ) {}

    public static function makeFromRequest(StoreUpdateBusinessListAdditionalPhones $request): self
    {
        return new self(
            $request->lista_comercial_complementar_id,
            $request->telefone,
        );
    }
}
