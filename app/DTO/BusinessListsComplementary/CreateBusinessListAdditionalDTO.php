<?php

namespace App\DTO\BusinessListsComplementary;

use App\Http\Requests\StoreUpdateBusinessListAdditional;

class CreateBusinessListAdditionalDTO
{
    public function __construct(
        public string $lista_comercial_id,
        public string $cidade_id,
        public string $quantidade_cnpj_no_cpf,
        public string $email,
        public string $idade,
    ) {}

    public static function makeFromRequest(StoreUpdateBusinessListAdditional $request): self
    {
        return new self(
            $request->lista_comercial_id,
            $request->cidade_id,
            $request->quantidade_cnpj_no_cpf,
            $request->email,
            $request->idade,
        );
    }
}
