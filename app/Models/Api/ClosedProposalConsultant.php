<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClosedProposalConsultant extends Model
{
    use HasFactory;

    protected $table = 'proposta_fechada_consultores';

    protected $fillable = [
        'status_implantacao_id',
        'plano_id',
        'atend_enviar_proposta_id',
        'data_fechamento'
    ];
}
