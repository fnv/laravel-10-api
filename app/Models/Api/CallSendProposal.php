<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CallSendProposal extends Model
{
    use HasFactory;

    protected $table = 'atend_enviar_propostas';

    protected $fillable = [
        'atendimento_id',
        'atend_enviar_proposta_tipo_envio_id',
        'atend_enviar_proposta_status_id',
        'nome',
        'cpf',
        'proposta_anexo',
        'data_envio',
    ];

}
