<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CallReturn extends Model
{
    use HasFactory;

    protected $table = 'atendimento_retornos';

    protected $fillable = [
        'atendimento_id',
        'cpf',
        'nome',
        'data',
        'hora',
        'observacao'
    ];

}
