<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CallSendProposalObservation extends Model
{
    use HasFactory;

    protected $table = 'atend_enviar_proposta_observacaos';

    protected $fillable = [
        'atend_enviar_proposta_id',
        'observacao'
    ];
}
