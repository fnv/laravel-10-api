<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttendSendProposalStatus extends Model
{
    use HasFactory;

    protected $table = 'atend_enviar_proposta_statuses';

    protected $fillable = [
        'nome'
    ];
}
