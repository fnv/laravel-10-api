<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessListAdditional extends Model
{
    use HasFactory;

    protected $table = 'lista_comercial_complementar';

    protected $fillable = [
        'lista_comercial_id',
        'cidade_id',
        'quantidade_cnpj_no_cpf',
        'email',
        'idade'
    ];
}
