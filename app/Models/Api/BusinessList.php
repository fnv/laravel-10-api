<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessList extends Model
{
    use HasFactory;

    protected $table = 'lista_comercial';

    protected $fillable = [
        'cpf',
        'nome',
        'razao_social',
        'plano',
        'data_de_vigencia',
        'valor',
        'quantidade_vidas',
        'descricao'
    ];

}
