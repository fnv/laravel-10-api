<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClosedProposal extends Model
{
    use HasFactory;

    protected $table = 'proposta_fechadas';

    protected $fillable = [
        'proposta_fechada_consultore_id',
        'consultor_id_que_fechou',
        'razao_social',
        'cnpj',
        'cnpj_cartao_cnpj_anexo',
        'email',
        'vigencia',
        'carta_permanencia_anexo',
        'cod_porte',
        'telefone',
        'tipo_de_tabela_cotada_anexo',
        'valor_total_sem_iof'
    ];
}
