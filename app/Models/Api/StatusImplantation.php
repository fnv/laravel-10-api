<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusImplantation extends Model
{
    use HasFactory;

    protected $table = 'status_implantacaos';

    protected $fillable = [
        'nome'
    ];
}
