<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttendSendProposalTypeSending extends Model
{
    use HasFactory;

    protected $table = 'atend_enviar_proposta_tipo_envios';

    protected $fillable = [
        'nome'
    ];
}
