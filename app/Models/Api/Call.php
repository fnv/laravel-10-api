<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Call extends Model
{
    use HasFactory;

    protected $table = 'atendimentos';

    protected $fillable = [
        'lista_comercial_id',
        'user_id',
        'atendimento_status_id'
    ];

}
