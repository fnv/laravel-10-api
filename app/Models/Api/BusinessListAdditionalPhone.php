<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessListAdditionalPhone extends Model
{
    use HasFactory;

    protected $table = 'lista_comercial_complementar_tels';

    protected $fillable = [
        'lista_comercial_complementar_id',
        'telefone'
    ];

}
