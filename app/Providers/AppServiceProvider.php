<?php

namespace App\Providers;

use App\Repositories\{SupportEloquentORM,
    OperatorEloquentORM,
    StatusImplantationEloquentORM,
    ServiceStatusEloquentORM,
    AttendSendProposalStatusEloquentORM,
    AttendSendProposalTypeSendingEloquentORM,
    PlanEloquentORM,
    CallEloquentORM,
    CallReturnEloquentORM,
    CallSendProposalEloquentORM,
    CallSendProposalObservationEloquentORM,
    BusinessListEloquentORM,
    BusinessListAdditionalEloquentORM,
    BusinessListAdditionalPhonesEloquentORM,
    ClosedProposalConsultantEloquentORM,
    ClosedProposalEloquentORM,
    UserEloquentORM
};
use App\Repositories\{SupportRepositoryInterface,
    OperatorRepositoryInterface,
    StatusImplantationRepositoryInterface,
    ServiceStatusRepositoryInterface,
    AttendSendProposalStatusRepositoryInterface,
    AttendSendProposalTypeSendingRepositoryInterface,
    PlanRepositoryInterface,
    CallRepositoryInterface,
    CallReturnRepositoryInterface,
    CallSendProposalRepositoryInterface,
    CallSendProposalObservationRepositoryInterface,
    BusinessListRepositoryInterface,
    BusinessListAdditionalRepositoryInterface,
    BusinessListAdditionalPhonesRepositoryInterface,
    ClosedProposalConsultantRepositoryInterface,
    ClosedProposalRepositoryInterface,
    UserRepositoryInterface
};
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(
            SupportRepositoryInterface::class,
            SupportEloquentORM::class
        );

        $this->app->bind(
            OperatorRepositoryInterface::class,
            OperatorEloquentORM::class
        );

        $this->app->bind(
            StatusImplantationRepositoryInterface::class,
            StatusImplantationEloquentORM::class
        );

        $this->app->bind(
            ServiceStatusRepositoryInterface::class,
            ServiceStatusEloquentORM::class
        );

        $this->app->bind(
            AttendSendProposalStatusRepositoryInterface::class,
            AttendSendProposalStatusEloquentORM::class
        );

        $this->app->bind(
            AttendSendProposalTypeSendingRepositoryInterface::class,
            AttendSendProposalTypeSendingEloquentORM::class
        );

        $this->app->bind(
            PlanRepositoryInterface::class,
            PlanEloquentORM::class
        );

        $this->app->bind(
            CallRepositoryInterface::class,
            CallEloquentORM::class
        );

        $this->app->bind(
            CallReturnRepositoryInterface::class,
            CallReturnEloquentORM::class
        );

        $this->app->bind(
            CallSendProposalRepositoryInterface::class,
            CallSendProposalEloquentORM::class
        );

        $this->app->bind(
            CallSendProposalObservationRepositoryInterface::class,
            CallSendProposalObservationEloquentORM::class
        );

        $this->app->bind(
            BusinessListRepositoryInterface::class,
            BusinessListEloquentORM::class
        );

        $this->app->bind(
            BusinessListAdditionalRepositoryInterface::class,
            BusinessListAdditionalEloquentORM::class
        );

        $this->app->bind(
            BusinessListAdditionalPhonesRepositoryInterface::class,
            BusinessListAdditionalPhonesEloquentORM::class
        );

        $this->app->bind(
            ClosedProposalConsultantRepositoryInterface::class,
            ClosedProposalConsultantEloquentORM::class
        );

        $this->app->bind(
            ClosedProposalRepositoryInterface::class,
            ClosedProposalEloquentORM::class
        );

        $this->app->bind(
            UserRepositoryInterface::class,
            UserEloquentORM::class
        );


    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
