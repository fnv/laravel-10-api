<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('lista_comercial_complementar_tels', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('lista_comercial_complementar_id')->nullable();
            $table->string('telefone', 255)->nullable();
            $table->timestamps();

            $table->foreign('lista_comercial_complementar_id','lcc_tels_lcc_id_foreign')->references('id')->on('lista_comercial_complementar');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('lista_comercial_complementar_tels');
    }
};
