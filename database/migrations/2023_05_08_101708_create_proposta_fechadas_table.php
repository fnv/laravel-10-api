<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('proposta_fechadas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('proposta_fechada_consultore_id')->nullable();
            $table->unsignedBigInteger('consultor_id_que_fechou')->nullable();
            $table->string('razao_social', 255)->nullable();
            $table->string('cnpj', 20)->nullable();            
            $table->string('cnpj_cartao_cnpj_anexo', 255)->nullable();
            $table->string('email', 255)->nullable();            
            $table->date('vigencia')->nullable();
            $table->string('carta_permanencia_anexo', 255)->nullable();
            $table->string('cod_porte', 255)->nullable();
            $table->string('telefone', 20)->nullable();
            $table->string('tipo_de_tabela_cotada_anexo', 255)->nullable();
            $table->decimal('valor_total_sem_iof', 10,2)->nullable();
            $table->timestamps();

            $table->foreign('proposta_fechada_consultore_id','pfc_id_pf_foreign')->references('id')->on('proposta_fechada_consultores');
            $table->foreign('consultor_id_que_fechou','c_id_pf_foreign')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('proposta_fechadas');
    }
};
