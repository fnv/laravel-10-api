<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('atend_enviar_propostas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('atendimento_id')->nullable();
            $table->unsignedBigInteger('atend_enviar_proposta_tipo_envio_id')->nullable();
            $table->unsignedBigInteger('atend_enviar_proposta_status_id')->nullable();
            $table->string('nome', 255)->nullable();
            $table->string('cpf', 20)->nullable();
            $table->string('proposta_anexo', 255)->nullable();
            $table->dateTime('data_envio')->nullable();
            $table->timestamps();

            $table->foreign('atendimento_id')->references('id')->on('atendimentos');
            $table->foreign('atend_enviar_proposta_tipo_envio_id','aepte_id_foreign')->references('id')->on('atend_enviar_proposta_tipo_envios');
            $table->foreign('atend_enviar_proposta_status_id','aeps_id_foreign')->references('id')->on('atend_enviar_proposta_statuses');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('atend_enviar_propostas');
    }
};
