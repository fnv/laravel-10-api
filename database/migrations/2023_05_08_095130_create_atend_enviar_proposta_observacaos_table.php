<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('atend_enviar_proposta_observacaos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('atend_enviar_proposta_id')->nullable();
            $table->longText('observacao')->nullable();
            $table->timestamps();

            $table->foreign('atend_enviar_proposta_id','aep_id_foreign')->references('id')->on('atend_enviar_propostas');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('atend_enviar_proposta_observacaos');
    }
};
