<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('proposta_fechada_vidas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('proposta_fechada_id')->nullable();
            $table->unsignedBigInteger('operadora_id')->nullable();
            $table->unsignedBigInteger('grau_parentesco_id')->nullable();
            $table->string('nome', 255)->nullable();
            $table->string('cpf', 20)->nullable();              
            $table->date('data_nascimento')->nullable();
            $table->enum('reembolso', ['Sim','Não'])->default('Não');
            $table->timestamps();

            $table->foreign('proposta_fechada_id','pf_id_pfv_foreign')->references('id')->on('proposta_fechadas');
            $table->foreign('operadora_id','o_id_pfv_foreign')->references('id')->on('operadoras');
            $table->foreign('grau_parentesco_id','gp_id_pfv_foreign')->references('id')->on('grau_parentescos');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('proposta_fechada_vidas');
    }
};
