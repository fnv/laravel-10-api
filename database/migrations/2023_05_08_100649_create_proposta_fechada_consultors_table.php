<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('proposta_fechada_consultores', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('status_implantacao_id')->nullable();
            $table->unsignedBigInteger('plano_id')->nullable();
            $table->unsignedBigInteger('atend_enviar_proposta_id')->nullable();
            $table->dateTime('data_fechamento')->nullable();
            $table->timestamps();

            $table->foreign('status_implantacao_id','si_id_foreign')->references('id')->on('status_implantacaos');
            $table->foreign('plano_id')->references('id')->on('planos');
            $table->foreign('atend_enviar_proposta_id','aepc_id_foreign')->references('id')->on('atend_enviar_propostas');
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('proposta_fechada_consultores');
    }
};
