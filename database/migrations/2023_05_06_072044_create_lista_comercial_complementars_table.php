<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('lista_comercial_complementar', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('lista_comercial_id')->nullable();
            $table->unsignedBigInteger('cidade_id')->nullable();
            $table->integer('quantidade_cnpj_no_cpf')->nullable();
            $table->string('email', 255)->nullable();
            $table->integer('idade')->nullable();
            $table->timestamps();

            $table->foreign('lista_comercial_id')->references('id')->on('lista_comercial');
            $table->foreign('cidade_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('lista_comercial_complementar');
    }
};
