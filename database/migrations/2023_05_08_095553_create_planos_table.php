<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('planos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('operadora_id')->nullable();
            $table->string('nome', 255);
            $table->decimal('valor', 10,2)->nullable();
            $table->timestamps();

            $table->foreign('operadora_id')->references('id')->on('operadoras');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('planos');
    }
};
