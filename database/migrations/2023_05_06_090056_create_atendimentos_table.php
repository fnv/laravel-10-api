<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('atendimentos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('lista_comercial_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('atendimento_status_id')->nullable();
            $table->timestamps();

            $table->foreign('lista_comercial_id')->references('id')->on('lista_comercial');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('atendimento_status_id')->references('id')->on('atendimento_status');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('atendimentos');
    }
};
