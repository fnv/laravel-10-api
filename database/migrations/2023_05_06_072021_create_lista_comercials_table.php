<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('lista_comercial', function (Blueprint $table) {
            $table->id();
            $table->string('cpf', 20);
            $table->string('nome', 255);
            $table->string('razao_social', 255);
            $table->string('plano', 255)->nullable();
            $table->date('data_de_vigencia')->nullable();
            $table->decimal('valor', 10,2)->nullable();
            $table->integer('quantidade_vidas')->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('lista_comercial');
    }
};
