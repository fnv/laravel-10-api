<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('atendimento_retornos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('atendimento_id')->nullable();
            $table->string('cpf', 20)->nullable();
            $table->string('nome', 255)->nullable();
            $table->date('data')->nullable();
            $table->time('hora')->nullable();
            $table->longText('observacao')->nullable();
            $table->timestamps();            

            $table->foreign('atendimento_id')->references('id')->on('atendimentos');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('atendimento_retornos');
    }
};
